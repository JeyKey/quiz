//
//  Answer.swift
//  Quiz
//
//  Created by Jarosław Krajewski on 22/06/16.
//  Copyright © 2016 Jarosław Krajewski. All rights reserved.
//

import Foundation

class Answer{
    let text:String
    let isCorrect:Bool
    
    init(_ content:String,isCorrect:Bool){
        self.text = content
        self.isCorrect = isCorrect
    }
    
    convenience init(_ content:String){
        self.init(content,isCorrect: false)
    }
    
}