//
//  QuizTableViewController.swift
//  Quiz
//
//  Created by Jarosław Krajewski on 24/06/16.
//  Copyright © 2016 Jarosław Krajewski. All rights reserved.
//

import UIKit
import CoreData

class QuizViewCotroller: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    @IBOutlet var quizNameLabel: UILabel!
    @IBOutlet var questionLabel: UILabel!
    
    // progress of completing quiz
    @IBOutlet var progressView: UIProgressView!
    
    @IBOutlet var collectionView: UICollectionView!
    
    // quiz data
    var quiz:Quiz!
    
    // quiz data from database
    private var quizDatabaseData: QuizData?
    private var questions = [Question]()
    
    var activeQuestionNumber:Int{
        get{
            return quiz.completedQuestions
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let jsonParser = JsonParser()
        
        //download questions from endpoint
        WebRequests.getRequestWithJsonSerialization("http://quiz.o2.pl/api/v1/quiz/\(self.quiz.id)/0", callback: {
            (data) in
            
            //parses questions from  JSON
            jsonParser.setData(data)
            self.questions = jsonParser.parseQuestions()
            
            
            let appDelegate = (UIApplication.shared().delegate as! AppDelegate)
            let context = appDelegate.managedObjectContext
            
            if let quizData = QuizData.quizDataWithId(context, id: self.quiz.id){
                self.quizDatabaseData = quizData
            }
            else {
                self.quizDatabaseData = QuizData(id: self.quiz.id, goodAnswers: 0, completedQuestions: 0, context: context)
            }
        
            self.updateQuestionInformation()
            self.adjustLabelsToScreen()
            
        })
        quizNameLabel.text = quiz.title
        
    }
    
    /**
     Set size of labels font to fit different screen sizes
     */
    func adjustLabelsToScreen() {
        
        let aspectRatio = CGFloat(0.15/5.0)
        self.quizNameLabel.font = self.quizNameLabel.font.withSize(aspectRatio * UIScreen.main().bounds.height)
        self.questionLabel.font = self.questionLabel.font.withSize(aspectRatio * UIScreen.main().self.bounds.height)
        self.questionLabel.sizeToFit()
        self.quizNameLabel.sizeToFit()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if questions.count > 0{
            return questions[activeQuestionNumber].answers.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "answerCell", for: indexPath) as! AnswerCell
    
        cell.answer = questions[activeQuestionNumber].answers[(indexPath as NSIndexPath).row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        //dynamic size
        return CGSize(width: collectionView.frame.width * 0.5 - 10, height: collectionView.frame.height * 0.5 - 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if questions[activeQuestionNumber].answers[(indexPath as NSIndexPath).row].isCorrect == true{
            //selected answers was good
            quiz.goodAnswers += 1
        }
        quiz.completedQuestions += 1
        
        
        if quiz.isCompleted{
            //quiz is finished. Save result show quiz result information
            quiz.finish()
            quizDatabaseData?.updateDatabase(quiz)
            
            performSegue(withIdentifier: "showQuizResultSegue", sender: quiz)
        }
        else{
            updateQuestionInformation()
        }
    }
    
    /**
     Updates information about question, question text.
     */
    func updateQuestionInformation(){
        
        quizDatabaseData?.updateDatabase(quiz)
        
        questionLabel.text = questions[activeQuestionNumber].text
        collectionView.reloadData()
        progressView.setProgress(quiz.progress, animated: true)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: AnyObject?) {
     
        let controller = segue.destinationViewController as! ResultViewController
        controller.quiz = sender as! Quiz
    }
    

}
