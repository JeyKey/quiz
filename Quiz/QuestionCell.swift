//
//  QuestionCell.swift
//  Quiz
//
//  Created by Jarosław Krajewski on 23/06/16.
//  Copyright © 2016 Jarosław Krajewski. All rights reserved.
//

import UIKit

class QuestionCell: UITableViewCell {

    var questionData:Question!
    
    var question:Question{
        set{
            self.questionData = newValue
            self.textLabel!.text  = questionData.text
        }
        get{
            return self.questionData
        }
    }
    
}
