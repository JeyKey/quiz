//
//  ResultViewController.swift
//  Quiz
//
//  Created by Jarosław Krajewski on 26/06/16.
//  Copyright © 2016 Jarosław Krajewski. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController {

    @IBOutlet var congratulationLabel: UILabel!
    @IBOutlet var youAnsweredLabel: UILabel!
    @IBOutlet var resultLabel: UILabel!
    @IBOutlet var questionsLabel: UILabel!
    
    @IBOutlet var showAllQuizzesButton: UIButton!
    @IBOutlet var repeatQuizButton: UIButton!
    
    var quiz:Quiz!
    override func viewDidLoad() {
        super.viewDidLoad()

        // setting dynamic font size for each label
        let aspectRatio = CGFloat(0.15/5.0)
        congratulationLabel.font = congratulationLabel.font.withSize(aspectRatio * UIScreen.main().bounds.height)
        youAnsweredLabel.font = youAnsweredLabel.font.withSize(aspectRatio * UIScreen.main().bounds.height)
        resultLabel.font = resultLabel.font.withSize(aspectRatio*2 * UIScreen.main().bounds.height)
        questionsLabel.font = questionsLabel.font.withSize(aspectRatio * UIScreen.main().bounds.height)
        
        //setting dynamic font size for each button
        showAllQuizzesButton.titleLabel!.font = showAllQuizzesButton.titleLabel!.font.withSize(aspectRatio * UIScreen.main().bounds.height)
        repeatQuizButton.titleLabel!.font = repeatQuizButton.titleLabel!.font.withSize(aspectRatio * UIScreen.main().bounds.height)
        
        //button label alighment to center
        showAllQuizzesButton.titleLabel?.textAlignment = NSTextAlignment.center
        repeatQuizButton.titleLabel!.textAlignment = NSTextAlignment.center
        
        
        resultLabel.text = "\(Int(Float(quiz.lastResult!)/Float(quiz.questions)*100))%"
        
    }
    
    //repeatQuiz button was pressed -> quiz start again
    @IBAction func repeatQuiz(_ sender: AnyObject) {
        performSegue(withIdentifier: "repeatQuizSegue", sender: self)
    }
    
    // returning to main screen
    @IBAction func showAllQuizzes(_ sender: AnyObject) {
        performSegue(withIdentifier: "showQuizzesSegue", sender: self)
    }
    

    
    override func prepare(for segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "repeatQuizSegue" {
            let controller = segue.destinationViewController as! QuizViewCotroller
            controller.quiz = quiz
        }
        
    }
 

}
