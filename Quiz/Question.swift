//
//  Question.swift
//  Quiz
//
//  Created by Jarosław Krajewski on 22/06/16.
//  Copyright © 2016 Jarosław Krajewski. All rights reserved.
//

import Foundation

class Question{
    let text:String
    var answers:[Answer]
    
    var answersCount:Int{
        get{
            return answers.count
        }
    }
    
    init(_ question:String,answers:[Answer]){
        self.text = question
        self.answers = answers
    }
    /**
     checks if question is correct -> has only one good answer
     
     - returns: true is is correct. Otherwise false
     */
    func hasOnlyOneGoodAnswer() -> Bool {
        var goodAnswersCount = 0
        
        //iterate over answers ans cound how many of them are correct
        for answer in answers {
            if answer.isCorrect{
                goodAnswersCount += 1
            }
        }
        if goodAnswersCount == 1 {
            return true
        }
        return false
    }
    
    /**
     adds answer to answers array
     
     - parameter answer: answer to be added
     */
    func addAnswer(_ answer:Answer)  {
        answers.append(answer)
    }
}
