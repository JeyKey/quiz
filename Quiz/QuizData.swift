//
//  quizData.swift
//  Quiz
//
//  Created by Jarosław Krajewski on 26/06/16.
//  Copyright © 2016 Jarosław Krajewski. All rights reserved.
//

import Foundation
import CoreData

class QuizData: NSManagedObject{
    
    let entityName = "Quiz"
    var context:NSManagedObjectContext!
    
    @NSManaged var id: NSNumber?
    @NSManaged var goodAnswers: NSNumber?
    @NSManaged var completedQuestions: NSNumber?
    @NSManaged var lastResult: NSNumber?
    
    
    init (id: NSNumber,goodAnswers:NSNumber,completedQuestions:NSNumber,context:NSManagedObjectContext){
        let entity = "Quiz"
        super.init(entity: NSEntityDescription.entity(forEntityName: entity, in: context)!, insertInto: context)
        self.id = id
        self.completedQuestions = completedQuestions
        self.goodAnswers = goodAnswers
        self.context = context
        self.lastResult = nil
        
    }
    
    override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }
    
    /**
     Search for quiz with passed id in database
     
     - Parameter context:   context
     - Parameter id: ID of searched quiz.
     
     - Returns: QuizData if entry exists. Otherwise nil
     */
    class func quizDataWithId(_ context:NSManagedObjectContext, id:NSNumber) -> QuizData?{
        
        let request = NSFetchRequest(entityName: "Quiz")
        request.returnsObjectsAsFaults = false
        request.predicate = Predicate(format: "id = %@", id)
        
        let results = try! context.fetch(request)
        if results.count > 0 {
            let quizData = results[0] as! QuizData
            quizData.context = context
            return quizData
        }
        else{
            // there are no quizzes with passed id
            return nil
        }
    }
    
    /**
     Search for quiz with passed id in database
     
     - Parameter quiz:  quiz with newer Data
     */
    func updateDatabase(_ quiz: Quiz){
        self.completedQuestions = quiz.completedQuestions
        self.goodAnswers = quiz.goodAnswers
        self.lastResult = quiz.lastResult
        
        do{
            //updating data in database
            try context.save()
        }
        catch let error{
            print(error)
        }
    }
    
}
