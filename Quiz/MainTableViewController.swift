//
//  MainTableViewController.swift
//  Quiz
//
//  Created by Jarosław Krajewski on 24/06/16.
//  Copyright © 2016 Jarosław Krajewski. All rights reserved.
//

import UIKit

class MainTableViewController: UITableViewController {

    var quizzes = [Quiz]()
    override func viewDidLoad() {
        super.viewDidLoad()

        // downloads quiz information from endpoint
        let parser = JsonParser()
        WebRequests.getRequestWithJsonSerialization("http://quiz.o2.pl/api/v1/quizzes/0/100", callback:{
            (data) in
            
            //parses requested data
            parser.setData(data)
            self.quizzes = parser.parseQuizzes()
            self.tableView.reloadData()
            
            //checks if information about quizzes are in database
            let appDelegate = (UIApplication.shared().delegate as! AppDelegate)
            let context = appDelegate.managedObjectContext
            for quiz in self.quizzes {
                
                //check is quiz id exists in database
                if let quizData = QuizData.quizDataWithId(context, id: quiz.id){
                    quiz.goodAnswers = quizData.goodAnswers as! Int
                    quiz.completedQuestions = quizData.completedQuestions as! Int
                    quiz.lastResult = quizData.lastResult as? Int
                }
            }

        })
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return quizzes.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell:QuizCell = QuizCell()
        
        // 1 layout for odd cell 1 for even
        switch (indexPath as NSIndexPath).row % 2{
        case 0:
            cell  = tableView.dequeueReusableCell(withIdentifier: "quizCell", for: indexPath) as! QuizCell
        case 1:
            cell  = tableView.dequeueReusableCell(withIdentifier: "reverseQuizCell", for: indexPath) as! QuizCell
        default: break
        }
        cell.quiz = quizzes[(indexPath as NSIndexPath).row]
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //dynamic size
        let aspectRatio = CGFloat(1.0/5.0)
        return aspectRatio * UIScreen.main().bounds.size.height
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: AnyObject?) {
        let quizController = segue.destinationViewController as!QuizViewCotroller
        let clickedCell = sender as! QuizCell
        quizController.quiz = clickedCell.quiz
    }
 

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // change view to selected quiz
        performSegue(withIdentifier: "quizSelection", sender: tableView.cellForRow(at: indexPath))
    }

}
