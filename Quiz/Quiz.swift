//
//  Quiz.swift
//  Quiz
//
//  Created by Jarosław Krajewski on 22/06/16.
//  Copyright © 2016 Jarosław Krajewski. All rights reserved.
//

import Foundation

class Quiz {

    let title:String
    let id:NSNumber
    let questions:Int
    var imgSrc:String?
    var lastResult:Int?
    var goodAnswers = 0
    var completedQuestions = 0
    
    var progress:Float{
        get{
            return Float(completedQuestions)/Float(questions)
        }
    }
    
    var isCompleted:Bool{
        get{
            if progress == 1{
                return true
            }
            else{
                return false
            }
        }
    }
    
    init(title:String,id:NSNumber,questions:Int){
        self.title = title
        self.id = id
        self.questions = questions
    }
    
    func finish(){
        lastResult = goodAnswers
        completedQuestions = 0
        goodAnswers = 0
    }
    
    
}