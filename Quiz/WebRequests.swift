//
//  WebRequests.swift
//  Quiz
//
//  Created by Jarosław Krajewski on 23/06/16.
//  Copyright © 2016 Jarosław Krajewski. All rights reserved.
//


import Foundation


class WebRequests {
    /**
     Sends GET request to passed url and serialize to JSON.
     
     - parameter urlString: url
     - parameter callback:  func that should be called after getting response from request
     */
    class func getRequestWithJsonSerialization(_ urlString:String, callback:(AnyObject?) ->Void ){
        
        getRequest(urlString, callback: {
            (data) in
            do{
                let json = try JSONSerialization.jsonObject(with: data! as! Data, options: []) as AnyObject?
                DispatchQueue.main.async(execute: { () -> Void in
                    
                    callback(json)
                })
            }
            catch let error{
                print(error)
            }

        })
        
    }
    
    /**
     GET request to passed url
     
     - parameter urlString: url
     - parameter callback:  func that should be called after getting response from request
     */
    class func getRequest(_ urlString:String, callback:(AnyObject?) ->Void ){

        let request = URLRequest(url: URL(string: urlString)!)
        
        URLSession.shared().dataTask(with: request) { (data, response, error) -> Void in DispatchQueue.main.async(execute: { () -> Void in
                
                callback(data)
            })
            
        }.resume()
    }
}


