//
//  JsonParser.swift
//  Quiz
//
//  Created by Jarosław Krajewski on 23/06/16.
//  Copyright © 2016 Jarosław Krajewski. All rights reserved.
//

import Foundation


class JsonParser {
    var root:[String:AnyObject]?
    
    init(){
        
    }
    
    /**
     Set data of JSON document
     
     - parameter root: JSON root
     */
    func setData(_ root:AnyObject?) {
        self.root = root as? [String:AnyObject]
    }
    
    /**
     Parses Quizzes
     
     - returns: quizzes from JSON document
     */
    func parseQuizzes() -> [Quiz] {
        var quizzes = [Quiz]()
        
        for quizData in root!["items"] as! [[String:AnyObject]]{
            
            //retrieving Quiz iformation from JSON
            let quizTitle = quizData["title"] as! String
            let quizID = quizData["id"] as! NSNumber
            let quizQuestions = quizData["questions"] as! Int
            let quiz = Quiz(title: quizTitle, id: quizID, questions: quizQuestions)
            
            //retrieving image url
            let photoData = quizData["mainPhoto"] as! [String:AnyObject]
            quiz.imgSrc = photoData["url"] as? String
            
            quizzes.append(quiz)
        }
        return quizzes
    }
    
    /**
     Parse questions with answers
     
     - returns: parsed question from JSON document
     */
    func parseQuestions() -> [Question] {
        var questions:[Question] = []
        
        //retrieving question information from JSON
        for question in root!["questions"] as! [[String:AnyObject]] {
            let questionTitle = question["text"] as! String
            
            //retrieving answers From JSON
            let answers = parseAnswers(question)
            questions.append(Question(questionTitle,answers:answers))
        }
        
        return questions
    }
    
    /**
     Parse answers from question Json
     
     - parameter question: question JSON element
     
     - returns: array of parsed answers
     */
    private func parseAnswers(_ question:AnyObject) ->[Answer]{
        var answers:[Answer] = []
        
        //retrieving answers information form JSON
        for answer in question["answers"] as! [[String:AnyObject]] {
            
            let text = "\(answer["text"]!)"
            if let isCorrect = answer["isCorrect"] as? Bool {
                answers.append(Answer(text,isCorrect: isCorrect))
            }
            else{
                answers.append(Answer(text))
            }
        }
        return answers
    }
}
