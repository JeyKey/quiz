//
//  AnswerCell.swift
//  Quiz
//
//  Created by Jarosław Krajewski on 23/06/16.
//  Copyright © 2016 Jarosław Krajewski. All rights reserved.
//

import UIKit

class AnswerCell: UICollectionViewCell {

    
    @IBOutlet var textLabel: UILabel!
    var answerData:Answer!
    var answer: Answer{
        set{
            self.answerData = newValue
            self.textLabel?.text = answerData.text
            
            //dynamic Cell size
            let aspectRatio = CGFloat(0.5/5.0)
            textLabel.font = textLabel.font.withSize(aspectRatio * self.bounds.height)
        }
        get{
            return answerData
        }
    }
}
