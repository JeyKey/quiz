//
//  QuizCell.swift
//  Quiz
//
//  Created by Jarosław Krajewski on 23/06/16.
//  Copyright © 2016 Jarosław Krajewski. All rights reserved.
//

import UIKit

class QuizCell: UITableViewCell {

    var quizData:Quiz!
    
    @IBOutlet var quizTitleLabel: UILabel!
    @IBOutlet var quizImage: UIImageView!
    
    @IBOutlet var completionLabel: UILabel!
    
    var quiz:Quiz{
        set{
            self.quizData = newValue
            quizTitleLabel.text = quizData.title
            
            if let lastResult = quizData.lastResult{
                // quiz was completed Before
                completionLabel.text = "Ostatni wynik \(lastResult)/\(quizData.questions) \(Int(Float(lastResult)/Float(quizData.questions)*100))%"
            }
            else{
                // quiz is still in progress
                completionLabel.text = "Quiz rozwiązany w \(Int(quizData.progress*100))%"
                completionLabel.isHidden = false
            }
            
            // download and set image from URL
            if let url = quizData.imgSrc{
                setImageFromURL(url)
            }
            
            //dynamic font size
            let aspectRatio = CGFloat(0.5/5.0)
            completionLabel.font = completionLabel.font.withSize(aspectRatio * self.bounds.height)
            quizTitleLabel.font = completionLabel.font
        }
        get{
            return self.quizData
        }
    }
    
    /**
     Download image from url and set as quizImage
     
     - parameter urlString: url of image
     */
    private func setImageFromURL(_ urlString:String){
        
        WebRequests.getRequest(urlString, callback: {
            (data) in
            self.quizImage.image = UIImage(data: data! as! Data)
        })
    }
}
