//
//  JsonRequestsTests.swift
//  Quiz
//
//  Created by Jarosław Krajewski on 23/06/16.
//  Copyright © 2016 Jarosław Krajewski. All rights reserved.
//

import XCTest

class WebRequestsTests: XCTestCase {
    
    
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testGetRequesting() {
        var expectation =   self.expectation(withDescription: "GetRequest")
        WebRequests.getRequest("http://d.wpimg.pl/259446039--1779706125/television.png",callback:{
            (data) in
            
            let image  = UIImage(data: data! as! Data)
            XCTAssert(image!.size.width == 639)
            expectation.fulfill()
        })
        waitForExpectations(withTimeout: 5000,handler:nil)
    }
    func testGetRequestingWithJsonSerialization() {
        
        var expectation =   self.expectation(withDescription: "GetRequest")
        WebRequests.getRequestWithJsonSerialization("http://quiz.o2.pl/api/v1/quizzes/0/100",callback:{
            (data) in
            
            XCTAssertTrue(data!["count"] != nil)
            expectation.fulfill()
        })
        waitForExpectations(withTimeout: 5000,handler:nil)
        
        expectation =   self.expectation(withDescription: "GetRequest")
        WebRequests.getRequestWithJsonSerialization("http://quiz.o2.pl/api/v1/quiz/6007290011906689/0",callback:{
            (data) in
            
            XCTAssertTrue("\(data!["id"])" == "6007290011906689")
            expectation.fulfill()
        })
        
        waitForExpectations(withTimeout: 5000,handler:nil)
        
        
    }
    
    
    
}
