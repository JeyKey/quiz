//
//  JsonParserTests.swift
//  Quiz
//
//  Created by Jarosław Krajewski on 23/06/16.
//  Copyright © 2016 Jarosław Krajewski. All rights reserved.
//

import XCTest

class JsonParserTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

   
    func testQuestionParse()  {
        let expectation = self.expectation(withDescription: "Parser")
        let jsonParser = JsonParser()
        WebRequests.getRequestWithJsonSerialization("http://quiz.o2.pl/api/v1/quiz/6007290011906689/0", callback: { (data) in
                jsonParser.setData(data)
                expectation.fulfill()
        })
        waitForExpectations(withTimeout: 1000, handler: nil)
        let questions = jsonParser.parseQuestions()
        XCTAssertTrue(questions.count == 7)
        for question in questions {
            XCTAssertTrue(question.hasOnlyOneGoodAnswer())
        }
        
    }

    func testQuizParse(){
        let expectation = self.expectation(withDescription: "Parser")
        let jsonParser = JsonParser()
        WebRequests.getRequestWithJsonSerialization("http://quiz.o2.pl/api/v1/quizzes/0/100", callback: { (data) in
            jsonParser.setData(data)
            expectation.fulfill()
        })
        waitForExpectations(withTimeout: 1000, handler: nil)
        let quizzes = jsonParser.parseQuizzes()
        
        XCTAssertTrue(quizzes.count == 100)
       

    }
}
