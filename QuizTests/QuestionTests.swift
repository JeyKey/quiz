//
//  QuestionTests.swift
//  Quiz
//
//  Created by Jarosław Krajewski on 22/06/16.
//  Copyright © 2016 Jarosław Krajewski. All rights reserved.
//

import XCTest

class QuestionTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testInit(){
        let question = Question("Kto to zrobił?",answers:[Answer("ja"),Answer("on"),Answer("ona")])
        XCTAssert(question.text == "Kto to zrobił?")
        XCTAssert(question.answersCount == 3)
    }
    
    func testQuestionHasOnlyOneGoodAnswer(){
        let question = Question("Kto to zrobił?",answers: [Answer("ja"),Answer("on"),Answer("ona")])
        let question2 = Question("Kto to zrobił?",answers: [Answer("ja",isCorrect: true),Answer("on",isCorrect: true),Answer("ona")])
        let question3 = Question("Kto to zrobił?",answers: [Answer("ja",isCorrect: true),Answer("on"),Answer("ona")])
        XCTAssertFalse(question.hasOnlyOneGoodAnswer())
        XCTAssertFalse(question2.hasOnlyOneGoodAnswer())
        XCTAssertTrue(question3.hasOnlyOneGoodAnswer())
    }
    
    func testAddingAnswers(){
        let question = Question("Who made this?",answers: [])
        XCTAssertTrue(question.answersCount == 0)
        question.addAnswer(Answer("Me"))
        XCTAssertTrue(question.answersCount == 1)
        question.addAnswer(Answer("You"))
        XCTAssertTrue(question.answersCount == 2)
        question.addAnswer(Answer("She"))
        XCTAssertTrue(question.answersCount == 3)
    }

}
