//
//  AnswerTests.swift
//  Quiz
//
//  Created by Jarosław Krajewski on 22/06/16.
//  Copyright © 2016 Jarosław Krajewski. All rights reserved.
//

import XCTest

class AnswerTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testInit() {
        let answer = Answer("1")
        XCTAssert(answer.text == "1")
    }
    
    func testCorrectAnswer(){
        let answer = Answer("2", isCorrect:true)
        XCTAssert(answer.isCorrect == true)
    }
    
    func testIncorrectAnswer(){
        let answer = Answer("3")
        XCTAssert(answer.isCorrect == false)
    }
}

