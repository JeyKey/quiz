//
//  QuizTests.swift
//  QuizTests
//
//  Created by Jarosław Krajewski on 22/06/16.
//  Copyright © 2016 Jarosław Krajewski. All rights reserved.
//

import XCTest
@testable import Quiz

class QuizTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testInit(){
        let quiz = Quiz(title: "tytul",id: 1234,questions:2)
        XCTAssert(quiz.title == "tytul")
        XCTAssert("\(quiz.id)" == "1234")
        XCTAssert(quiz.questions == 2)
    }
    

    
       
}
